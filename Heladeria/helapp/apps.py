from django.apps import AppConfig


class HelappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'helapp'
